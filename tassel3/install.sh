#!/bin/bash
# This program is to set up a cloud image for GBS SNP calling using a modified
# version of TASSEL3 GBS UNEAK. This version is modified to account for
# combinatorial bar codes and remove the read-through bar code if exists. The
# bar codes are hardcoded in the modified code. Enzymes are TEGApekI TEGPstI
# and TEGEcoT22i.
# It depends on a standard Debian 9 cloud instance.

# Script dependencies
sudo apt-get -y install rename

# install some things java 6 requires
sudo apt-get -y install libasound2 libx11-6 libxext6 libxi6 libxt6 libxtst6

# Switch to /tmp for external packages downloading.
cd /tmp

# Get my copy of Java 6
wget --no-check-certificate "https://data.elshiregroup.co.nz/s/9yZZKE4LQQpyM3W" -P /tmp
mv /tmp/download /tmp/oracle-java6-jdk_6u45_amd64.deb

# Auotmatically accept java license so this script can run automatically.
echo oracle-java6-installer shared/accepted-oracle-license-v1-1 select true | sudo /usr/bin/debconf-set-selections

# Install java 6
sudo dpkg -i /tmp/oracle-java6-jdk_6u45_amd64.deb

# Move to /opt for installation of the software
cd /opt

# Get the version of TASSEL3 with modified enzymes
sudo wget --no-check-certificate "https://data.elshiregroup.co.nz/index.php/s/Ny8QYjPCswYpqMH" -P /tmp

sudo mv /tmp/download /tmp/tassel3.0.174-TEGenzymes.v2.tar.gz

# Unpack TASSEL3
sudo tar -xzf /tmp/tassel3.0.174-TEGenzymes.v2.tar.gz -C /opt

# Make files readable and executable by all user
sudo chmod -R 755 /opt/tassel3-src

# TASSEL3 needs to be called using /opt/tassel3-src/run_pipeline.pl in the scripts. This is because it needs
# to access the libs which it thinks are relative to its position.

# Try to run the pipeline without arguments and see if it returns the parseArgs string.
# If it does, then put GFANZ_TEST_RESULTS=OK into syslog. If it does not, then 
# put GFANZ_TEST_RESULTS=ERROR into syslog.

/opt/tassel3-src/run_pipeline.pl | grep parseArgs >> /dev/null

if [ $? -eq 0 ]; then
  echo "GFANZ_TEST_RESULTS=OK"
else
  echo "GFANZ_TEST_RESULTS=ERROR"
fi
