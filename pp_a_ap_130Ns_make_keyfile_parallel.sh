#!/bin/bash

[ -n "$1" ] || { echo "Usage: pp_a_ap_130Ns_make_keyfile_parallel.sh PROJECTID LANENUMBER"; exit 1; }

set -e
set -u
set -o pipefail


#    		Copyright 2018 Robert J. Elshire
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Depends: https://github.com/kdmurray91/seqhax
#          GNU parallel
#
#
# Assumption is that the sequence files are demultiplexed with axedemux and each sequence file
# contains sequences from one individual sample only. 
# This script creates a directory called seq_modified and then processes all of the 
# sequence files in the current directory. It prepends an A to the sequence  (to act as a fake bar code),
# appends 130 Ns (to pad out the length for TASSEL), trims back to 130 (so the reads aren't excessively long),
#  gzips the files and generates a key file for use with TASSEL UNEAK.

# Note: This script benefits from many available cores. It does not have high RAM requirements.

# Add project name variable and add that to the keyfile below FIXME

PROJECTID=$1
LANENUMBER=$2

# prepare demultiplexed files for modification.

# Create a demultiplexed folder

mkdir demultiplexed

cd demultiplexed
    
# create file links in demultplexed folder

ln -s ../"R1"$PROJECTID"_"*"R1"* ./

# rename the files to follow the TASSEL Convention (ProjectID_SampleName_s_LANE_fastq.gz)

rename  's/R1\./s_1_/g' *.fastq.gz

mkdir seq_modified

#This does the work by processing the files in parallel
ls -1 ./*_fastq.gz | parallel 'seqhax preapp -P A {} | seqhax preapp -S NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN /dev/stdin | seqhax trunc -l 130 /dev/stdin | gzip > ./seq_modified/{}'

# generate a key file for these things
cd seq_modified
echo -e "Flowcell\tLane\tBarcode\tFullSampleName\tPlateName\tRow\tColumn\tFlowCellID\tLibraryPrepID" > keyfile.txt
for file in *_fastq.gz; do var="${file%_s_"$LANENUMBER"_fastq.gz}" ;echo -e "${var#*_}\t$LANENUMBER\tA\t${var#*_}\t1\tA\t1\t\t "   >> keyfile.txt; done

