# GBS Pipeline Scripts

These are a set of scripts for working with raw sequence data generated by The Elshire Group. The run scripts and install scripts are in this repo. 

**Note:** These tools and instructions are provided as a courtesy only. If you do not understand fully how the tools work in relation to the project you are using them on, we strongly advise you to seek expert assistance.

# Elshire Group GBS SNP Calling Tools
## Demultiplexing
This step separates the combined sequences into files based on well position. The script to install this tool is here: https://gitlab.com/relshire/gbs-pipeline-scripts/tree/master/axe-demultiplexer

**Warning:** Always use this tool with the -m0 flag set. See the notes in the [axe-demux folder](https://gitlab.com/relshire/gbs-pipeline-scripts/tree/master/axe-demultiplexer).

### Run Script Name
run_axedemultiplexer.sh
## Sequence Modification
This step makes the sequences all the same length in preparation for SNP calling and generates a SNP calling keyfile template. The script to install this tool is here: https://gitlab.com/relshire/gbs-pipeline-scripts/tree/master/seqhax
### Run Script Name
pp_a_ap_130Ns_make_keyfile_parallel.sh
## Key File
The template is made in the previous step. See below for instructions how to edit this file so that it works for your project.
## Reference Based Tassel 5 (TEG mod)
The script to install the TEG modified version of Tassel 5 is here: https://gitlab.com/relshire/gbs-pipeline-scripts/tree/master/tassel5
### Prepare Reference
Various aligners can be used at this stage. If using bowtie2, when using the bowtie2-build command the input files need to be uncompressed fasta files. These need to be entered in a comma separated list with no spaces.

### Run Script Name
run_tassel5_GBSv2.sh
## DeNovo Tassel 3 UNEAK (TEG mod)
The script to install the TEG modified version of Tassell 3 is here: https://gitlab.com/relshire/gbs-pipeline-scripts/tree/master/tassel3
### Run Script Name
run_tassel3_UNEAK.sh

# Elshire Group GBS SNP Calling Example Workflow

1.  Put the raw sequence files and the bar code key file in a directory and go to that directory. Then run:
    1.  run_axedemultiplexer.sh *ProjectID* *BarCodeKeyFileName*
1.  Then in the same directory where the demultiplexed files are run:
    1.  pp_a_ap_130Ns_make_keyfile_parallel.sh *ProjectID* *LaneNumber*
1.  Then manually edit the keyfile to change the entries in the column FullSampleName to be the sample names for your project in the corresponding wells.
1. Proceed with either Tassel5 GBS V2 for reference based or Tassel 3 UNEAK for denovo SNP calling. In both cases the hard coded values in the scripts **must be modified to suit your experimental conditions**. If this is unclear, please see the **Note** above.
