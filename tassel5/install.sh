#!/bin/bash
# This program is to set up a cloud image for GBS SNP calling
# using a modified versio of TASSEL5 GBS V2. This version is
# modified to account for combinatorial bar codes and remove
# the read-through bar code if exists. The bar codes are hard
# coded in the modified code. Enzymes are TEGApekI TEGPstI and TEGEcoT22i
# It depends on a standard Debian 9 cloud instance.

# The following packages are dependecies for TASSEL5 and scripts we expect to use.
sudo apt-get -y install openjdk-8-jdk-headless bowtie2

# Move to /opt for installation of the software
cd /opt

# Get the version of TASSEL5 with modified enzymes
sudo wget --no-check-certificate  "https://data.elshiregroup.co.nz/s/FEf6twZKL4WH3aR" -P /tmp
sudo mv /tmp/download /tmp/tassel5-TEGenzymes.v.2.tar.gz

# Unpack TASSEL5
sudo tar -xzf /tmp/tassel5-TEGenzymes.v.2.tar.gz -C /opt

# Make files readable and executable by all user
sudo chmod -R 755 /opt/tassel5-src

# TASSEL5 needs to be called using /opt/tassel5-src/run_pipeline.pl in the scripts. This is because it needs
# to access the libs which it thinks are relative to its position.

# Try to run the pipeline without arguments and see if it returns the parseArgs string.
# If it does, then put GFANZ_TEST_RESULTS=OK into syslog. If it does not, then 
# put GFANZ_TEST_RESULTS=ERROR into syslog.

/opt/tassel5-src/run_pipeline.pl | grep parseArgs >> /dev/null

if [ $? -eq 0 ]; then
  echo "GFANZ_TEST_RESULTS=OK"
else
  echo "GFANZ_TEST_RESULTS=ERROR"
fi
