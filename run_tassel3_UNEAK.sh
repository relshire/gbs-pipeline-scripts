#!/bin/bash

[ -n "$1" ] || { echo "Usage: run_UNEAK.sh PROJECTID ENZYME KEYFILE_LOCATION SEQUENCE_FILE_LOCATION"; exit 1; }
[ -n "$2" ] || { echo "Usage: run_UNEAK.sh PROJECTID ENZYME KEYFILE_LOCATION SEQUENCE_FILE_LOCATION"; exit 1; }
[ -n "$3" ] || { echo "Usage: run_UNEAK.sh PROJECTID ENZYME KEYFILE_LOCATION SEQUENCE_FILE_LOCATION"; exit 1; }
[ -n "$4" ] || { echo "Usage: run_UNEAK.sh PROJECTID ENZYME KEYFILE_LOCATION SEQUENCE_FILE_LOCATION"; exit 1; }

set -e
set -u
set -o pipefail


#    		Copyright 2018 Robert J. Elshire
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Note the specific flags for the TASSEL 3 UNEAK pipeline below will need to be changed for your specific project.
# DO NOT just use this script without understanding what you are doing.


PROJECTID=$1
ENZYME=$2
KEYFILE_LOCATION=$3
SEQUENCE_FILE_LOCATION=$4

DATE=$(date +%Y%m%d) #Date from system in the format YYYYMMDD

# Create the folders for UNEAK

time ~/opt/tassel3-src/run_pipeline.pl -fork1 -UCreatWorkingDirPlugin -w ./ -endPlugin -runfork1 2>&1 | tee -a ./"$PROJECTID"_TASSEL3_UNEAK_Pipeline_"$DATE".log

# Soft Link to Keyfile KEYFILE_LOCATION (full path)

ln -s $KEYFILE_LOCATION ./key/keyfile.txt

# Soft link to Sequences

ln -s "$SEQUENCE_FILE_LOCATION"/*_fastq.gz ./Illumina/
    
#Run tassel pipeline, redirect stderr to stdout, copy stdout to log file.

time ~/opt/tassel3-src/run_pipeline.pl -Xms1G -Xmx7G -fork1 -UFastqToTagCountPlugin -w ./ -e $ENZYME -endPlugin -runfork1 2>&1 | tee -a ./"$PROJECTID"_TASSEL3_UNEAK_Pipeline_"$DATE".log

time ~/opt/tassel3-src/run_pipeline.pl -Xms1G -Xmx7G -fork1 -UMergeTaxaTagCountPlugin -w ./ -c 5 -endPlugin -runfork1 2>&1 | tee -a ./"$PROJECTID"_TASSEL3_UNEAK_Pipeline_"$DATE".log

time ~/opt/tassel3-src/run_pipeline.pl -Xms1G -Xmx7G -fork1 -UTagCountToTagPairPlugin -w ./ -e 0.03 -endPlugin -runfork1 2>&1 | tee -a ./"$PROJECTID"_TASSEL3_UNEAK_Pipeline_"$DATE".log

time ~/opt/tassel3-src/run_pipeline.pl -Xms1G -Xmx7G -fork1 -UTagPairToTBTPlugin -w ./ -endPlugin -runfork1 2>&1 | tee -a ./"$PROJECTID"_TASSEL3_UNEAK_Pipeline_"$DATE".log

time ~/opt/tassel3-src/run_pipeline.pl -Xms1G -Xmx7G -fork1 -UTBTToMapInfoPlugin -w ./ -endPlugin -runfork1 2>&1 | tee -a ./"$PROJECTID"_TASSEL3_UNEAK_Pipeline_"$DATE".log

time ~/opt/tassel3-src/run_pipeline.pl -Xms1G -Xmx7G -fork1 -UMapInfoToHapMapPlugin -w ./ -mnMAF 0.05 -mxMAF 0.5 -mnC 0 -mxC 1 -endPlugin -runfork1 2>&1 | tee -a ./"$PROJECTID"_TASSEL3_UNEAK_Pipeline_"$DATE".log

# rename the hapmap files with PROJECTID and date

cd hapMap

# FIXME The rename below does not work right it adds the ' try this manually and sort it out.

rename "s/HapMap/'$PROJECTID'_'$DATE'_HapMap/g" *.txt

# FIXME Add feature to run genosummary to get the key metrics and chuck into a file

# gzip the hapmap files

gzip *.txt
