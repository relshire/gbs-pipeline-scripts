#!/bin/bash
set -e
set -u
set -o pipefail

# Dependencies: debian 9, axe-demux.

# This script assumes that you are in the directory where you have the sequence 
# files and that the keyfile is located in the same directory.
# The keyfile contains bar code pairs and well ID numbers.
# The well ID numbers will be part of the output file names.
# USAGE: run_axedemultiplexer.sh ProjectID Keyfile
# The expected output will be a demultiplexed sequence files.

# Set Variables with readable names

PROJECTID=$1
KEYFILE=$2


# Demultiplex the Sequence Files. This assumes one pair of paired end files per directory.
# Output demultiplexed sequences in 2 files (non-interleaved) named RXPROJECTID_WELL_RX.fastq.gz

axe-demux -m0 -z 3 -c -2 -v -t "$PROJECTID"_summary.txt -b $KEYFILE -f *_R1.fastq.gz -r *_R2.fastq.gz -F R1"$PROJECTID" -R R2"$PROJECTID"

# Make a summary directory and move the summary stats file there

mkdir summary_stats

mv "$PROJECTID"_summary.txt summary_stats

# Make an unknown directory and move the unknown sequences there

mkdir unknown_sequences

mv *"$PROJECTID"_unknown* unknown_sequences
