#!/bin/bash

set -u
set -o pipefail

# This program is to set up a cloud image for GBS Data demultiplexing using combinatorial bar codes.

# It depends on a standard Debian 9 cloud instance.

sudo apt-get update

sudo apt-get -y upgrade

# The following line is for manual use of the image or testing purposed.

sudo apt-get -y install byobu htop

# install demultiplexer

sudo apt-get -y install  axe-demultiplexer

# Try to run axe-demux with -V and see if it returns AXE in the string.
# If it does, then put GFANZ_TEST_RESULTS=OK into syslog. If it does not, then 
# put GFANZ_TEST_RESULTS=ERROR into syslog.

axe-demux -V | grep AXE >> /dev/null

if [ $? -eq 0 ]
then
  logger GFANZ_TEST_RESULTS=OK
else
  logger GFANZ_TEST_RESULTS=ERROR
fi
