# Axe Demultiplexer

## Description

Axe demultiplexer is used to demultiplex bar coded sequences. It can process combinatorial bar codes from paired end reads. 

## Source

https://github.com/kdmurray91/axe

Or via the debian package axe-demultiplexer

## Documentation

https://axe-demultiplexer.readthedocs.io/en/latest/

## Paper Reference

Preprint: https://www.biorxiv.org/content/early/2017/07/07/160606

Publication: https://doi.org/10.1093/bioinformatics/bty432

## Notes

Always use with the mismatch flag set to -m0 rather than the default. There are issues with the differences in assumptions between the software used to generate the adapters and the axe-demux. The software used to generate the adapters considers the enzyme cut site while axe-demux does not. This can (and sometimes does) lead to assigning reads to the wrong sample if the default is used.
