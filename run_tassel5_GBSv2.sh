#!/bin/bash

[ -n "$1" ] || { echo "Usage: run_GBSV2.sh PROJECTID ENZYME KEYFILE_LOCATION SEQUENCE_FILE_LOCATION REFGENPATH STARTCHR ENDCHR"; exit 1; }
[ -n "$2" ] || { echo "Usage: run_GBSV2.sh PROJECTID ENZYME KEYFILE_LOCATION SEQUENCE_FILE_LOCATION REFGENPATH STARTCHR ENDCHR"; exit 1; }
[ -n "$3" ] || { echo "Usage: run_GBSV2.sh PROJECTID ENZYME KEYFILE_LOCATION SEQUENCE_FILE_LOCATION REFGENPATH STARTCHR ENDCHR"; exit 1; }
[ -n "$4" ] || { echo "Usage: run_GBSV2.sh PROJECTID ENZYME KEYFILE_LOCATION SEQUENCE_FILE_LOCATION REFGENPATH STARTCHR ENDCHR"; exit 1; }
[ -n "$5" ] || { echo "Usage: run_GBSV2.sh PROJECTID ENZYME KEYFILE_LOCATION SEQUENCE_FILE_LOCATION REFGENPATH STARTCHR ENDCHR"; exit 1; }

set -e
set -u
set -o pipefail

# Note the specific flags for the TASSEL GBS V2 pipeline below will need to be changed for your specific project.
# DO NOT just use this script without understanding what you are doing.

PROJECTID=$1
ENZYME=$2
KEYFILE_LOCATION=$3 #The name of the keyfile needs to be in the form of PROJECTID_SNPCalling_keyfile.txt
SEQUENCE_FILE_LOCATION=$4
REFGENDIRPATH=$5
REFGENPREFIX=$6
SCHROM=$7
ECHROM=$8


DATE=$(date +%Y%m%d) #Date from system in the format YYYYMMDD

PROCESSORS=$(nproc)  #Number of processors on system

# Test to see if they exist and if not, Create directory structure

test ! -d ./sequences && mkdir sequences

test ! -d ./keyfile && mkdir keyfile

test ! -d ./refgen && mkdir refgen

test ! -d ./alignment && mkdir alignment


# Softlink Sequence Files

ln -s $SEQUENCE_FILE_LOCATION/*_fastq.gz ./sequences/

# Copy Key Files

cp $KEYFILE_LOCATION/"$PROJECTID"_SNPCalling_keyfile.txt ./keyfile/

# Copy RefGen Files

cp $REFGENDIRPATH/* ./refgen/


# Run GBSSeqToTagDBPlugin

time /opt/tassel5-src/run_pipeline.pl -Xms1G -Xmx12G -fork1 -GBSSeqToTagDBPlugin -e $ENZYME -i ./sequences  -db ./"$PROJECTID".db   -k ./keyfile/"$PROJECTID"_SNPCalling_keyfile.txt -kmerLength 120 -minKmerL 20 -mnQS 20 -mxKmerNum 100000000 -batchSize 31 -endPlugin -runfork1  2>&1 | tee -a ./"$PROJECTID"_GBSV2_Pipeline_"$DATE".log

# Run TagExportToFastqPlugin

time /opt/tassel5-src/run_pipeline.pl -Xms1G -Xmx12G -fork1 -TagExportToFastqPlugin -db ./"$PROJECTID".db -o ./"$PROJECTID"tagsForAlign.fa.gz -c 5 -endPlugin -runfork1 2>&1 | tee -a ./"$PROJECTID"_GBSV2_Pipeline_"$DATE".log


# Align

time bowtie2 -p $PROCESSORS --very-sensitive -x ./refgen/$REFGENPREFIX -U "$PROJECTID"tagsForAlign.fa.gz -S ./alignment/"$PROJECTID"_tagsForAlignFullvs.sam 2>&1 | tee -a ./"$PROJECTID"_GBSV2_Pipeline_"$DATE".log

# run SAMToGBSdbPlugin Creates the initial DB of tags

time /opt/tassel5-src/run_pipeline.pl -Xms1G -Xmx12G -fork1 -SAMToGBSdbPlugin -i ./alignment/"$PROJECTID"_tagsForAlignFullvs.sam -db  ./"$PROJECTID".db -aProp 0.0 -aLen 0 -endPlugin -runfork1 2>&1 | tee -a ./"$PROJECTID"_GBSV2_Pipeline_"$DATE".log

# run DiscoverySNPCallerPluginV2

time /opt/tassel5-src/run_pipeline.pl -Xms1G -Xmx12G -fork1 -DiscoverySNPCallerPluginV2 -db  ./"$PROJECTID".db -sC $SCHROM -eC $ECHROM -mnLCov 0.1 -mnMAF 0.01 -deleteOldData true -endPlugin -runfork1   2>&1 | tee -a ./"$PROJECTID"_GBSV2_Pipeline_"$DATE".log

# run ProductionSNPCallerPluginV2

time /opt/tassel5-src/run_pipeline.pl -Xms1G -Xmx12G -fork1 -ProductionSNPCallerPluginV2 -db ./"$PROJECTID".db -e $ENZYME -i ./sequences -k ./keyfile/"$PROJECTID"_SNPCalling_keyfile.txt -kmerLength 120 -o ./"$PROJECTID"productionHapMap"$DATE".vcf -batchSize 31 -endPlugin -runfork1 2>&1 | tee -a ./"$PROJECTID"_GBSV2_Pipeline_"$DATE".log

# run GetTagTaxaDistFromDBPlugin

time /opt/tassel5-src/run_pipeline.pl -Xms1G -Xmx12G -fork1 -GetTagTaxaDistFromDBPlugin -db ./"$PROJECTID".db -o ./"$PROJECTID"_TagTaxaDistOutput"$DATE".txt -endPlugin -runfork1 2>&1 | tee -a ./"$PROJECTID"_GBSV2_Pipeline_"$DATE".log

# run the genotypeSummary plugin to generate summary statistics.

time /opt/tassel5-src/run_pipeline.pl -Xmx12g -importGuess "$PROJECTID"productionHapMap"$DATE".vcf -genotypeSummary overall,site -export genotypeSummary 

# Add some things to GZIP the output files from the above steps.

gzip *.vcf

gzip ./alignment/*

gzip *.txt
