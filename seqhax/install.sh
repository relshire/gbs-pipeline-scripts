#!/bin/bash 
# This program is to set up a cloud image for the tool seqhax.
# It depends on a standard Debian 9 cloud instance.

# The following packages are dependecies for seqhax and scripts we expect to use.
# seqhax operates on single files but can take advantage of multiple cores using parallel.
sudo apt-get -y install byobu htop git zlib1g-dev cmake build-essential libboost-dev parallel

# Download and install seqhax
cd /tmp
git clone https://github.com/kdmurray91/seqhax.git
cd seqhax
mkdir build && cd build
cmake ..
make

sudo make install

# Try to run axe-demux with -V and see if it returns AXE in the string.
# If it does, then put GFANZ_TEST_RESULTS=OK into syslog. If it does not, then 
# put GFANZ_TEST_RESULTS=ERROR into syslog.
seqhax | grep seqhax >> /dev/null

if [ $? -eq 0 ]; then
  echo "GFANZ_TEST_RESULTS=OK"
else
  echo "GFANZ_TEST_RESULTS=ERROR"
fi
